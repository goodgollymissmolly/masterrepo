view: order_items {
  sql_table_name: public.order_items ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [raw, day_of_week_index] ## you can have other timeframes here too
    sql: ${TABLE}.created_at
      ;;
  }

  dimension_group: stop {
    type: time
    timeframes: [raw, day_of_week_index] ## same here!
    sql: ${TABLE}.delivered_at
      ;;
  }

dimension: weekday_count {
  type: number
  sql: DATEDIFF('day', ${start_raw}, ${stop_raw}) - ((FLOOR(DATEDIFF('day', ${start_raw}, ${stop_raw}) / 7) * 2) +
CASE WHEN DATE_PART(dow, ${start_raw}) - DATE_PART(dow, ${stop_raw}) IN (1, 2, 3, 4,5) AND DATE_PART(dow, ${stop_raw}) != 0
THEN 2 ELSE 0 END +
CASE WHEN DATE_PART(dow, ${start_raw}) != 0 AND DATE_PART(dow, ${stop_raw}) = 0
THEN 1 ELSE 0 END +
CASE WHEN DATE_PART(dow, ${start_raw}) = 0 AND DATE_PART(dow, ${stop_raw}) != 0
THEN 1 ELSE 0 END) ;;
}


  dimension: weekend_count {
    type: number
    sql: ((FLOOR(DATEDIFF('day', ${start_raw}, ${stop_raw}) / 7) * 2) +
      CASE WHEN DATE_PART(dow, ${start_raw}) - DATE_PART(dow, ${stop_raw}) IN (1, 2, 3, 4,5) AND DATE_PART(dow, ${stop_raw}) != 0
      THEN 2 ELSE 0 END +
      CASE WHEN DATE_PART(dow, ${start_raw}) != 0 AND DATE_PART(dow, ${stop_raw}) = 0
      THEN 1 ELSE 0 END +
      CASE WHEN DATE_PART(dow, ${start_raw}) = 0 AND DATE_PART(dow, ${stop_raw}) != 0
      THEN 1 ELSE 0 END) ;;
  }
  dimension_group: created {
    type: time
    timeframes: [
      raw,
      day_of_week,
      day_of_week_index,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: delivered {
    type: time
    timeframes: [
      raw,
      day_of_week,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.delivered_at ;;
  }

  dimension: inventory_item_id {
    type: number
    sql: ${TABLE}.inventory_item_id ;;
  }

  dimension: order_id {
    type: number
    sql: ${TABLE}.order_id ;;
  }

  dimension_group: returned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.returned_at ;;
  }

  dimension: sale_price {
    type: number
    sql: ${TABLE}.sale_price ;;
  }

  dimension_group: shipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.shipped_at ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: user_id {
    type: number
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
