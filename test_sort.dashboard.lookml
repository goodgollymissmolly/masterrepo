# - name: add_a_unique_name_1527030577
#   title: Untitled Visualization
#   model: molly_testgit
#   explore: orders
#   type: table
#   fields: [users.created_date, users.id, users.name]
#   sorts: [calculation_1 desc]
#   limit: 500
#   dynamic_fields:
#   - table_calculation: calculation_1
#     label: Calculation 1
#     expression: if(${users.id}=4, yes, no)
#     value_format:
#     value_format_name:
#     _kind_hint: dimension
#     _type_hint: yesno
#   query_timezone: America/New_York
