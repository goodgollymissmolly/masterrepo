
view: precision_user {

  derived_table: {
    sql: SELECT
        1 as user_id,
        1 as order_id,
        "Molly" as user_name


      UNION ALL

      SELECT
      2 as user_id,
      1 as order_id,
      "Jeremy" as user_name


      UNION ALL

      SELECT
      3 as user_id,
      2 as order_id,
      "Emma" as user_name

     ;;
  }

  # Define your dimensions and measures here, like this:
  dimension: user_id {
    type: number
    sql: ${TABLE}.user_id ;;
  }

  dimension: order_id {
    type: number
    sql: ${TABLE}.order_id ;;
  }

  dimension: user_name {
    type: string

    sql: ${TABLE}.user_name ;;
  }

  measure: count {
    type: count

  }
}

view: precision_orders {

  derived_table: {
    sql: SELECT
        1 as order_id,
        1.12345678987654321 as order_amount


        UNION ALL

        SELECT
        2 as order_id,
        2.123456789876554321 as order_amount


        UNION ALL

        SELECT
        3 as order_id,
        3.12345678987654321 as order_amount

     ;;
  }

  dimension: order_id {
    primary_key:  yes
    type: number
    sql: ${TABLE}.order_id ;;
  }

  dimension: order_amount {
    type: number
    sql: ${TABLE}.order_amount ;;
  }

  measure: avg {
    type: average
    sql: ${order_amount} ;;
  }
}
